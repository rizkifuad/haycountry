import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'
import { wp } from '../utils'

import BoxListing from '../components/BoxListing';
import HeaderBar from '../components/HeaderBar';
import SearchBar from '../components/SearchBar';
import SearchBarRight from '../components/SearchBarRight';
import SearchModal from './Search';
import { NavigationActions } from 'react-navigation';

import { headerStyle } from '../styles'

//const data= [
  //{
    //title: 'Alrounder, Hunter, Weight, Carrier, Sport',
    //category: 'Horses',
    //image: require('../images/listings/img-06.jpg'),
    //price: '$595',
    //location: 'South East, Staplehurst',
    //direction: '56 km away from you',
    //liked: false
  //},
  //{
    //title: 'Alrounder, Hunter, Weight, Carrier, Sport',
    //category: 'Horses',
    //category: 'Horses',
    //image: require('../images/listings/img-07.jpg'),
    //price: '$595',
    //location: 'South East, Staplehurst',
    //direction: '56 km away from you',
    //liked: false
  //},
  //{
    //title: 'Alrounder, Hunter, Weight, Carrier, Sport',
    //category: 'Horses',
    //category: 'Horses',
    //image: require('../images/listings/img-08.jpg'),
    //price: '$595',
    //location: 'South East, Staplehurst',
    //direction: '56 km away from you',
    //liked: false
  //},
//]

const GoToListing = NavigationActions.navigate({
  routeName: 'HOME',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTING' }),
});

class Screen extends Component {
  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      headerLeft: <SearchBar count='2.153'/>,
      headerStyle: headerStyle,
      headerRight: <SearchBarRight onPress={() => params.toggleModal()} image={require('../images/listings_map/icon-filters.png')} />,
      headerMode: 'float'
    }
  }

  constructor(props) {
    super(props)
    const data = []
    this.state = { data, isModalVisible: false, isLoading: true }
    this.toggleModal = this.toggleModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.toggleLike = this.toggleLike.bind(this)
  }

  componentDidMount() {
    this.props.navigation.setParams({
      toggleModal: this.toggleModal
    });

    fetch('https://www.haycountry.com/search.json')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          data: responseJson.results,
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  toggleModal() {
    this.setState({ isModalVisible: !this.state.isModalVisible})
  }

  toggleLike(key) {
    let item = this.state.data
    item[key].liked = !item[key].liked
    this.setState({data: item})
  }

  closeModal() {
    this.setState({ isModalVisible: false})
  }

  goToListing() {
    this.props.navigation.dispatch(GoToListing)
  }

  render() {
    return (
      <View style={styles.container}>
      <ScrollView style={styles.scrollview}>
          <ImageBackground style={styles.mapBg} source={require('../images/listings_map/bg-map.jpg')} />
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity activeOpacity={0.75} style={styles.buttonSeeMap} onPress={() => this.props.navigation.navigate('MAP_LISTINGS')}>
                <Image style={styles.seeMapImage} source={require("../images/listings/icon-location_active.png")}/>
                <Text style={styles.seeMapText}>See results on map</Text>
            </TouchableOpacity>
          </View>
          <View style={{marginBottom:30}}>
          {
            this.state.data.map((d, i) => {
              return <BoxListing data={d} key={i} onToggleLike={() => {this.toggleLike(i)}} onPress={() => this.goToListing()} />
            })
          }
          </View>
      </ScrollView>
      <SearchModal isModalVisible={this.state.isModalVisible} closeModal={this.closeModal} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  scrollview: {
    width:wp(100),
  },
  mapBg: {
    position: 'absolute',
    width: wp(100),
    height: wp(54.66667),
  },
  buttonSeeMap: {
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 40,
    width: 250,
    backgroundColor: '#fff',
    elevation: 4,
    flexDirection: 'row',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 17,
    paddingBottom: 17,
    borderRadius: 6,

    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
  },
  seeMapText: {
    fontFamily: 'nunito-regular',
    fontSize: 18,
    marginLeft: 15,
    color: '#000'
  },
  seeMapImage: {
    marginTop: 5,
    width: 13,
    height: 16
  }
})


export default Screen;
