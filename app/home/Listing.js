import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  Image,
  ImageBackground,
  View, 
  ScrollView, 
  Button,
  Dimensions,
  PixelRatio,
  Linking
} from 'react-native'
import { wp, height } from '../utils'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MapView, { Marker } from 'react-native-maps';

import BoxListing from '../components/BoxListing';
import ContactModal from './Contact';

import { hays } from '../data'


const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;




const data= {
  title: 'Alfalfa Mixed Grass Large Round',
  category: {
    id: 1,
    name: 'Horses',
    ref: ''
  },
  image: [
    require('../images/single_listing/cover.jpg'),
    require('../images/single_listing/cover.jpg'),
    require('../images/single_listing/cover.jpg'),
  ],
  price: '$595',
  safe_address: 'South East, Staplehurst',
  distance_from: 56000,
  liked: false,
  latitude: 37.78825,
  longitude: -122.4324
}


class Screen extends Component {
  static navigationOptions = {
    header: null,
    headerMode: 'none'
  }

  constructor(props) {
    super(props)
    this.state = { data, others: hays, isModalVisible: false, activeSlide: 0 }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.toggleLike = this.toggleLike.bind(this)
    this.toggleRecommendedLike = this.toggleRecommendedLike.bind(this)
    this._renderItem = this._renderItem.bind(this)
  }

  componentDidMount() {
    this.props.navigation.setParams({
      toggleModal: this.toggleModal
    });
  }

  openModal() {
    this.setState({ isModalVisible: true})
  }

  toggleLike(key) {
    data.liked = !data.liked
    this.setState({data})
  }

  toggleRecommendedLike(key) {
    let items = this.state.others
    items[key].liked = !items[key].liked
    this.setState({others: items})
  }

  closeModal() {
    this.setState({ isModalVisible: false})
  }

  _renderItem ({item, index}) {
    return (
      <BoxListing data={item} onToggleLike={() => {this.toggleRecommendedLike(index)}}
        boxContainerStyle={{width: itemWidth, paddingHorizontal: itemHorizontalMargin}}
        boxStyle={{marginLeft: 3, marginRight: 3}}
        boxImageStyle={{height:wp(45)}}
        boxPriceStyle={{top: wp(39)}} />
    );
  }
  _renderItemTop({item, index}) {
    return <Image style={styles.boxImage} source={item} />
  }

  get pagination () {
    const { entries, activeSlide } = this.state;
    return (

      <Pagination
        dotsLength={this.state.data.image.length}
        activeDotIndex={activeSlide}
        containerStyle={styles.sliderPagination}
        dotStyle={styles.sliderIndicator}
        dotElement={<Image style={styles.sliderDot} source={require("../images/single_listing/icon-dot_active.png")}/>}
        inactiveDotElement={<Image style={styles.sliderDot}  source={require("../images/single_listing/icon-dot.png")}/>}
        carouselRef={this._carousel}
        tappableDots={!!this._carousel}
      />
    )
  }


  render() {
    const likedImage = this.state.data.liked ? require('../images/saved_listings/icon-like_active.png') : require('../images/home/icon-like.png')

    const pricePer = this.state.data.pricePer ? (
        <Text style={styles.pricePer}>
            { ' / ' + data.pricePer }
        </Text>
    ) : false;

    const marker = {
      latitude: this.state.data.latitude,
      longitude: this.state.data.longitude,
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollview}>
          <View style={styles.carouselContainer}>
            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.data.image}
              renderItem={this._renderItemTop}
              autoplay={true}
              sliderWidth={sliderWidth}
              itemWidth={sliderWidth}
              firstItem={0}
              inactiveSlideScale={1}
              inactiveSlideOpacity={1}
              containerCustomStyle={styles.slider}
              onSnapToItem={(index) => this.setState({ activeSlide: index }) }
              contentContainerCustomStyle={styles.sliderContentContainer}
            />

            <View style={styles.topInfo}>
              <TouchableOpacity style={styles.buttonBack} activeOpacity={0.75} onPress={() => this.props.navigation.goBack()}>
                <Image style={styles.backImage} source={require('../images/single_listing/icon-back-white.png')} />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonLike} onPress={this.toggleLike}>
                <Image style={styles.likeImage} source={likedImage}/>
              </TouchableOpacity>
            </View>

            <View style={styles.paginationWrap}>
              { this.pagination }
            </View>

            <View style={styles.videoBox}>
              <TouchableOpacity onPress={() => Linking.openURL('https://youtube.com')}>
                <Image style={styles.videoImage} source={require('../images/single_listing/icon-video.png')}/>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.boxDetail}>
            <Text style={styles.title}>{data.title}</Text>

            <View style={styles.boxLocation}>
              <Image style={styles.directionImage} source={require('../images/home/icon-location.png')}/>
              <View style={{marginLeft: 10}}>
                <Text style={styles.location}>{data.safe_address} ({parseInt(data.distance_from/1000)} km away from you)</Text>
              </View>
            </View>

            <Text style={styles.priceBox}>
              <Text style={styles.price}>$7-$5</Text>
              <Text style={styles.pricePer}> Depending on Quality / Bale</Text>
            </Text>

            <Text style={styles.header}>DESCRIPTION</Text>
            <Text style={styles.description}>
              It is a long established fact that a reader will be distracted by the readable content of a page
              when looking at it's layout
            </Text>
          </View>

          <View style={styles.extraInfo}>
            <View style={styles.itemInfo}>
              <Text style={styles.infoHeader}>AVAILABILITY</Text>
              <Text style={styles.infoDesc}>6000 bales</Text>
            </View>

            <View style={styles.itemInfo}>
              <Text style={styles.infoHeader}>FERTILIZED</Text>
              <Text style={styles.infoDesc}>No</Text>
            </View>

            <View style={styles.itemInfo}>
              <Text style={styles.infoHeader}>TYPE</Text>
              <Text style={styles.infoDesc}>ALFALFA</Text>
            </View>
          </View>

          <Text style={[styles.header, {paddingLeft: wp(6.66667), paddingRight: wp(6.66667)}]}>LOCATION</Text>
          <View style={styles.mapContainer}>
            <MapView
              style={styles.map}
              initialRegion={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}>
                <Marker
                  coordinate={marker}
                  image={require('../images/listings_map/icon-map-marker.png')}
                />
            </MapView>
          </View>

          <Text style={[styles.header, {paddingLeft: wp(6.66667), paddingRight: wp(6.66667)}]}>OTHER SIMILAR LISTINGS</Text>
          <View style={styles.carouselContainer}>
            <Carousel
              data={this.state.others}
              renderItem={this._renderItem}
              extraData={this.state}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
              firstItem={0}
              inactiveSlideScale={1}
              inactiveSlideOpacity={1}
              containerCustomStyle={styles.slider}
              contentContainerCustomStyle={styles.sliderContentContainer}
              loop={true}
              loopClonesPerSide={2}
            />
          </View>

        </ScrollView>

        <View style={styles.viewBottom}>
          <Text style={styles.priceWrapBottom}>
            <Text style={styles.priceBottom}>$15.00</Text>
            <Text style={styles.pricePerBottom}> / per bale</Text>
          </Text>

          <TouchableOpacity activeOpacity={0.75} style={styles.buttonContact} onPress={this.openModal}>
            <Image style={styles.contactImage} source={require("../images/single_listing/btn-green-round-sm-05.png")}/>
            <Text style={styles.contactText}>Contact Seller</Text>
          </TouchableOpacity>
        </View>

        <ContactModal isModalVisible={this.state.isModalVisible} closeModal={this.closeModal} />
      </View>
    )
  }
}

const entryBorderRadius = 8;
const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  scrollview: {
    flex: 1,
  },
  carouselContainer:{
    marginBottom:40,
  },
  boxImage: {
    width: wp(100),
    height: wp(83.46667)
  },
  topInfo: {
    position: 'absolute',
    left:0,
    width:viewportWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop:20,
    paddingLeft: wp(5),
    paddingRight: wp(5.5),
  },
  buttonBack: {
  },
  backImage: {
    width: 45,
    height: 45,
  },
  buttonLike: {
  },
  likeImage: {
    width: wp(11),
    height: wp(11),
  },
  paginationWrap: {
    position: 'absolute',
    bottom:0,
  },
  sliderPagination: {
    paddingVertical:15,
  },
  sliderIndicator: {
    padding:0,
    margin:0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sliderDot: {
    padding:0,
    margin:-7,
    width: 40,
    height: 40,
  },
  viewBottom: {
    width: "100%",
    zIndex:40,
    elevation: 19,
    backgroundColor: '#fff',
    paddingLeft: wp(6.6667),
    paddingRight: wp(6.6667),
    bottom:0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop:15,
    paddingBottom:15,

    shadowColor: '#000',
    shadowOpacity: 0.15,
    shadowOffset: { width: 0, height: -2 },
    shadowRadius: 2,
  },
  priceWrapBottom: {
    width: wp(43.33333),
    paddingTop:13,
  },
  priceBottom:{
    fontFamily: 'nunito-bold',
    color: '#000',
    fontSize: 20,
  },
  pricePerBottom: {
    color: '#000',
    fontSize: 12,
    fontFamily: 'nunito-regular'
  },
  buttonContact: {
    justifyContent:'center',
    alignItems: 'center',
    marginRight:wp(6.66667),
  },
  contactImage: {
    width: wp(42.53334),
    height: wp(14)
  },
  contactText: {
    position: 'absolute',
    fontFamily: 'nunito-regular',
    color: '#fff',
    fontSize: 17,
    marginLeft: 10,
    zIndex: 10
  },
  videoBox: {
    position: 'absolute',
    right: wp(6.66667),
    bottom: -30,
    elevation: 5,
    backgroundColor: '#fff',
    borderRadius:60,
    flexDirection: 'row',
    width:60,
    height:60,
    alignItems: 'center',
    justifyContent: 'center',

    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
  },
  videoImage: {
    width: 35,
    height: 35,
  },
  boxDetail: {
    paddingLeft: wp(6.66667),
    paddingRight: wp(6.66667),
    marginTop:-5,
  },
  title: {
    marginBottom: 13,
    fontSize: 26,
    lineHeight: 34,
    color: '#000',
    fontFamily: 'nunito-bold',
  },
  boxLocation: {
    width: wp(86.13334),
    flexDirection: 'row',
  },
  directionImage: {
    width: wp(4.26667),
    height: wp(4.26667),
    marginTop: 2
  },
  location: {
    color: '#7b7a7a',
    fontFamily: 'nunito-bold',
    fontSize: 14,
  },
  priceBox: {
    marginTop: 20,
  },
  price: {
    color: '#429e32',
    fontFamily: 'nunito-bold',
    fontSize: 19,
  },
  pricePer:{
    color:'#101010',
    fontFamily: 'nunito-regular',
    fontSize: 14,
  },
  header: {
    marginTop: 40,
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 18,
  },
  description: {
    marginTop: 5,
    lineHeight: 26,
    marginBottom: 10,
    color: '#000',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  extraInfo: {
    marginTop: 30,
    backgroundColor: '#f7f7f7',
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: wp(6.66667),
    paddingRight: wp(6.66667),
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  infoHeader: {
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 14,
  },
  infoDesc: {
    color: '#111',
    marginTop: 10,
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  mapContainer: {
    marginTop: 20,
    height: wp(70),
    width: "100%",
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },

})


export default Screen;
