import React, { Component } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity, StyleSheet, Dimensions, SafeAreaView, ImageBackground } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import BoxListing from '../components/BoxListing';

import HeaderBar from '../components/HeaderBar';
import SearchBar from '../components/SearchBar';
import SearchBarRight from '../components/SearchBarRight';
import SearchModal from '../explore/Search';
import { headerStyle } from '../styles';
import { NavigationActions } from 'react-navigation';
import { horses, hays, recommended } from '../data'


const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const GoExploreListing = NavigationActions.navigate({
  routeName: 'EXPLORE',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTINGS' }),
});

class Screen extends Component {
  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      headerLeft: <Image style={{left:0, alignItems: 'flex-start', marginLeft: wp(6.6667), width: 150, height: 26}} source={require('../images/logo.png')}/>,
      headerStyle: headerStyle,
      headerRight: <SearchBarRight onPress={() => params.toggleModal()} image={require('../images/home/icon-nav-explore.png')} />,
      headerMode: 'float',
    }
  }
  constructor(props) {
    super(props)
    this.toggleModal = this.toggleModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.toggleLike = this.toggleLike.bind(this)
    this._renderItem = this._renderItem.bind(this)
    this._renderItemHorse = this._renderItemHorse.bind(this)
    this._renderItemRecommended = this._renderItemRecommended.bind(this)
    this._renderItemHay = this._renderItemHay.bind(this)
    this.state = {
      isModalVisible: false,
      recentHorse: horses,
      recentHay: hays,
      Recommended: recommended
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      toggleModal: this.toggleModal
    });
  }

  toggleModal() {
    this.setState({ isModalVisible: !this.state.isModalVisible})
  }
  
  closeModal() {
    this.setState({ isModalVisible: false})
  }
  toggleLike(type) {
    let item = []
    let index = 0

    switch(type) {
      case 'horse':
        index = this._carouselHorses.currentIndex
        item = this.state.recentHorse
        item[index].liked = !item[index].liked
        this.setState({recentHorse: item})
        break;
      case 'hay':
        index = this._carouselHays.currentIndex
        item = this.state.recentHay
        item[index].liked = !item[index].liked
        this.setState({recentHay: item})
        break;
      case 'recommended':
        index = this._carouselRecommended.currentIndex
        item = this.state.Recommended
        item[index].liked = !item[index].liked
        this.setState({Recommended: item})
        break;
    }
  }
  

  _renderItemHorse(o) {
    return this._renderItem(o, 'horse')
  }

  _renderItemHay(o) {
    return this._renderItem(o, 'hay')
  }

  _renderItemRecommended(o) {
    return this._renderItem(o, 'recommended')
  }

  _renderItem ({item, index}, type) {
    //return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    return (
      <BoxListing data={item} key={index} onToggleLike={() => {this.toggleLike(type)}}
        onPress={() => {this.props.navigation.navigate('LISTING')}}
        boxContainerStyle={{width: itemWidth, paddingHorizontal: itemHorizontalMargin}}
        boxStyle={{marginLeft: 3, marginRight: 3}}
        boxImageStyle={{height:wp(45)}}
        boxPriceStyle={{top: wp(39)}} />
    );
  }

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
	      <View style={styles.container}>
	      	<ScrollView style={styles.scrollview} scrollEventThrottle={200} directionalLockEnabled={true}>
	      		<Text style={styles.header}>Recent Horse Listings</Text>
		        <View style={styles.carouselContainer}>
	            <Carousel
                extraData={this.state}
                ref={(c) => { this._carouselHorses = c; }}
	              data={this.state.recentHorse}
                renderItem={this._renderItemHorse}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                firstItem={0}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                containerCustomStyle={styles.slider}
                contentContainerCustomStyle={styles.sliderContentContainer}
                loop={true}
                loopClonesPerSide={2}
	            />
	        	</View>
            <View style={[styles.wrap, {alignItems: 'center', marginBottom:15}]}>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonOutline} onPress={() => this.props.navigation.dispatch(GoExploreListing)}>
                <Text style={styles.buttonOutlineText}>See all 253 horse listings</Text>
              </TouchableOpacity>
            </View>

	      		<Text style={styles.header}>Recent Hay Listings</Text>
		        <View style={styles.carouselContainer}>
	            <Carousel
                ref={(c) => { this._carouselHays = c; }}
                extraData={this.state}
	              data={this.state.recentHay}
                renderItem={this._renderItemHay}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                firstItem={0}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                containerCustomStyle={styles.slider}
                contentContainerCustomStyle={styles.sliderContentContainer}
                loop={true}
                loopClonesPerSide={2}
	            />
	        	</View>
            <View style={[styles.wrap, {alignItems: 'center', marginBottom:15}]}>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonOutline} onPress={() => this.props.navigation.dispatch(GoExploreListing)}>
                <Text style={styles.buttonOutlineText}>See all 1.347 hay listings</Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity activeOpacity={0.75} style={styles.buttonCTA} onPress={() => this.props.navigation.navigate('SUBMIT')}>
	            <ImageBackground style={styles.greenBg} source={require('../images/home/bg-01.jpg')}>
	            	<Text style={styles.greenTitle}>
	            		<Text>Reach</Text>
	            		<Text style={styles.greenTitleBold}> 5.000 ++ </Text>
	            		<Text>visitors every day!</Text>
	            	</Text>
	            	<Text style={styles.greenSubtitle}>Upload your listing now!</Text>
	            </ImageBackground>
            </TouchableOpacity>

	      		<Text style={styles.header}>Recommended For You</Text>
	      		<View style={styles.carouselContainer}>
	            <Carousel
                ref={(c) => { this._carouselRecommended = c; }}
                extraData={this.state}
	              data={this.state.Recommended}
                renderItem={this._renderItemRecommended}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                firstItem={0}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                containerCustomStyle={styles.slider}
                contentContainerCustomStyle={styles.sliderContentContainer}
                loop={true}
                loopClonesPerSide={2}
	            />
	        	</View>
            <View style={[styles.wrap, {alignItems: 'center', marginBottom:50}]}>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonOutline} onPress={() => this.props.navigation.dispatch(GoExploreListing)}>
                <Text style={styles.buttonOutlineText}>See more listings you might be interested in</Text>
              </TouchableOpacity>
            </View>

	      	</ScrollView>
	      </View>
        <SearchModal isModalVisible={this.state.isModalVisible} closeModal={this.closeModal} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
  	flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor:'#fff',
  },
  scrollview: {
    flex: 1,
  },
  wrap:{
    paddingLeft: wp(6.66667),
    paddingRight: wp(6.66667),
  },
  logo: {
    width: 175,
    height: 30
  },
  buttonSearch: {
    marginRight: wp(6.6667),
  },
  searchImage: {
    width: 30,
    height: 30
  },
  header: {
    marginTop: 30,
    marginBottom: 10,
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 18,
    paddingLeft: wp(6.66667),
    paddingRight: wp(6.66667),
  },
  carouselContainer:{
  	marginBottom:20,
  },
  slider: {
    overflow: 'visible' // for custom animations
  },
  sliderContentContainer: {
  },
  carouselItem:{
    width: viewportWidth,
    alignItems: 'center',
  },
  buttonOutline: {
  	width:wp(86.66666),
    padding: 13,
    borderRadius: 4,
    backgroundColor:'#fff',
    borderColor: '#cdcdcd',
    borderWidth: 1
  },
  buttonOutlineText: {
    color: '#1264ca',
    textAlign:'center',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  buttonCTA:{
    marginTop:40,
    marginBottom:10,
  },
  greenBg:{
    alignItems: 'center',
    justifyContent: 'center',
    height:wp(41.06667),
  },
  greenTitle:{
  	color:'#fff',
    fontFamily: 'nunito-regular',
    fontSize: 21,
    lineHeight:30,
    textAlign:'left',
    width:235,
  },
  greenTitleBold: {
    fontFamily: 'nunito-bold',
  },
  greenSubtitle:{
  	color:'rgba(255,255,255,.5)',
    fontFamily: 'nunito-regular',
    fontSize: 15,
    textAlign:'left',
    width:235,
    marginTop:8,
  }
})

export default Screen;
