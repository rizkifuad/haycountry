import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';
import MapView, { Marker } from 'react-native-maps';

import { HeaderBackButton } from 'react-navigation';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'

import { headerStyle } from '../styles'

class Screen extends Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle: 
    <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='New Listing' tagline="Step 3 of 4"/>
      </View>,
    headerStyle: headerStyle,
    headerTitleStyle: {alignSelf: 'center'},
    headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
    headerRight: <View/>
  })

  constructor(props) {
    super(props)
    this.state = {
      location: '',

      fillComplete: true

    }
  }

  render() {
    const buttonText = this.state.fillComplete ? "Move To Final Step" : "Enter Location To Move To Final Step"
    const buttonImage = this.state.fillComplete ? require('../images/contact_seller/btn-green.png') : require('../images/submit/btn-grey.png') 

    const marker = {
      latitude: 37.78825,
      longitude: -122.4324
    }


    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <View style={{marginBottom:50}}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop:40, marginBottom:20}}>
              <Image style={styles.infoImage} source={require('../images/submit/icon-location.png')}/>
              <Text style={styles.header}>LOCATION</Text>
            </View>
            <FormInput placeholder="Enter Location">
                <Image style={styles.gpsImage} source={require('../images/search/icon-gps.png')}/>
            </FormInput>
            <View style={styles.mapContainer}>
              <MapView
                style={styles.map}
                initialRegion={{
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}>
                  <Marker
                    coordinate={marker}
                    image={require('../images/listings_map/icon-map-marker.png')}
                  />
              </MapView>
            </View>
          </View>
        </ScrollView>

        <ButtonBottom text={buttonText} image={buttonImage} onPress={() => this.props.navigation.navigate("STEP4")}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  header: {
    marginLeft: 10,
    color: '#256fcd',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  gpsImage: {
    position: 'absolute',
    top: 18,
    right: 0,
    width: 25,
    height: 25,
  },
  infoImage: {
    width: 16,
    height: 16,
  },
  mapContainer: {
    marginTop: 20,
    height: wp(87),
    width: wp(87),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    overflow: 'hidden'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 20,
    overflow: 'hidden'
  },
})

export default Screen
