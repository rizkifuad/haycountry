import React, { Component } from 'react';
import Searches from '../saved/Searches'
import Listings from '../saved/Listings'
import { wp } from '../utils'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { NavigationActions } from 'react-navigation';
const GoToListing = NavigationActions.navigate({
  routeName: 'HOME',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTING' }),
});


const GoExploreListing = NavigationActions.navigate({
  routeName: 'EXPLORE',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTINGS' }),
});

const menu = [
  {text: 'Saved Listings', page: Listings, position: {justifyContent: 'flex-end', marginRight: wp(10)}},
  {text: 'Saved Searches', page: Searches, position: {justifyContent: 'flex-start', marginLeft: wp(8)}},
]

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const sliderWidth = width;
const itemWidth= width;

class Screen extends Component {
  constructor(props) {
    super(props)
    this.state = {activeSlide: 0}
    this.goToListing = this.goToListing.bind(this)
    this.goToExploreListing = this.goToExploreListing.bind(this)
    this.renderPage = this.renderPage.bind(this)
  }

  goToListing() {
    this.props.navigation.dispatch(GoToListing)
  }

  goToExploreListing() {
    this.props.navigation.dispatch(GoExploreListing)
  }


  renderPage({item, index}) {
    const Page = item.page
    return (
      <View style={styles.wrapper}>
        <Page aaa="meggila" goToListing={this.goToListing} goToExploreListing={this.goToExploreListing} />
      </View>
    )
  }


  render() {
    const menux = menu.slice(0)
    menux.reverse()

    return (
      <ScrollView style={styles.container}>
        <View style={[styles.menu, menu[this.state.activeSlide].position]}>
          {
            menux.map((m, i) => {
              let menuStyle = styles.link
              if (i !== this.state.activeSlide) {
                menuStyle = [styles.link, styles.linkActive]
              }
              return <Text style={menuStyle} key={i}>{m.text}</Text>
            })
          }
        </View>
        <Carousel
          ref={(c) => { this._carousel = c }}
          data={menu}
          renderItem={this.renderPage}
          sliderWidth={sliderWidth}
          onSnapToItem={(index) => this.setState({ activeSlide: index }) }
          itemWidth={itemWidth}
          inactiveSlideScale={.9}
          inactiveSlideOpacity={1}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    width: width,
    alignItems: 'center',
  },
  menu:{
    marginTop: 40,
    marginBottom: 25,
    flexDirection: 'row-reverse',
  },
  link:{
    color: "#c9c9c9",
    fontFamily: 'nunito-bold',
    fontSize: 26,
    marginRight: 30,
  },
  linkActive: {
    color: "#1b1b1b"
  }
})

export default Screen;
