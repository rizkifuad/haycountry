import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Listings from '../explore/Listings';
import MapListings from '../explore/MapListings';

export default StackNavigator({
  LISTINGS: {
    screen: Listings,
  },
  MAP_LISTINGS: {
    screen: MapListings,
  },
}, {
  initialRouteName: 'LISTINGS',
  mode: 'modal',
});

