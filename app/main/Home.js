import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Main from '../home/Main';
import Listing from '../home/Listing';

export default StackNavigator({
  MAIN: {
    screen: Main,
  },
  LISTING: {
    screen: Listing,
    navigationOptions: { tabBarVisible: false },
  },
}, {
  initialRouteName: 'MAIN',
  mode: 'modal',
});

