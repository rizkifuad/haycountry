import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Step1 from '../submit/Step1';
import Step2 from '../submit/Step2';
import Step3 from '../submit/Step3';
import Step4 from '../submit/Step4';

export default StackNavigator({
  STEP1: {
    screen: Step1,
  },
  STEP2: {
    screen: Step2,
  },
  STEP3: {
    screen: Step3,
  },
  STEP4: {
    screen: Step4,
  }
}, {
  navigationOptions: { tabBarVisible: false },
  initialRouteName: 'STEP1',
  mode: 'modal',
});

