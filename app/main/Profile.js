import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Profile from '../profile/Main';
import EditProfile from '../profile/EditProfile';
import BillingInfo from '../profile/BillingInfo';
import MyListings from '../profile/MyListings';
import Notification from '../profile/Notification';

import PlanUpgrade from '../profile/PlanUpgrade';

export default StackNavigator({
  MAIN: {
    screen: Profile,
    navigationOptions: { tabBarVisible: true } 
  },
  EDIT_PROFILE: {
    screen: EditProfile,
  },
  BILLING_INFO: {
    screen: BillingInfo,
  },
  MY_LISTINGS: {
    screen: MyListings,
  },
  NOTIFICATION: {
    screen: Notification,
  },
  PLAN_UPGRADE: {
    screen: PlanUpgrade,
  },
}, {
  navigationOptions: { tabBarVisible: false },
  initialRouteName: 'MAIN',
  mode: 'modal',
});

