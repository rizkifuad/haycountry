import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Onboarding from './front/Onboarding';
import SignIn from './front/SignIn';
import SignUp from './front/SignUp';
import Dashboard from './main/Dashboard';

const RootStack = StackNavigator({
  Onboarding: {
    screen: Onboarding,
  },
  SignIn: {
    screen: SignIn,
  },
  SignUp: {
    screen: SignUp,
  },
  Dashboard: {
    screen: Dashboard,
  },
}, {
  initialRouteName: 'Dashboard',
  mode: 'modal',
  headerMode: 'none',
});

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
