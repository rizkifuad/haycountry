import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { wp } from '../utils'

class SearchBar extends Component {
  render() {
    const taglineStyle = this.props.taglineColor ? {color: this.props.taglineColor} : {}
    return (
      <Text style={styles.baseText}>
        <Text style={styles.title}>{this.props.count}</Text>
        <Text style={styles.text}> results found</Text>
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  baseText: {
    marginLeft: wp(6.6777),
    color: '#000',
    textAlign: 'center',
  },
  title: {
    fontFamily: 'nunito-bold',
    fontSize: 20
  },
  text: {
    fontFamily: 'nunito-bold',
    fontSize: 16,
    color: '#8d8d8d'
  }

})


export default SearchBar
