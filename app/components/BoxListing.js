import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'

const entryBorderRadius = 8;
const styles = StyleSheet.create({
  boxContainer: {
  },
  box: {
    marginTop:15,
    marginBottom:25,
    marginLeft: wp(6.66667),
    marginRight: wp(6.66667),
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    borderRadius: entryBorderRadius,
    elevation: 8,
    backgroundColor:'#fff',
    //overflow: 'hidden'
  },
  boxImage: {
    borderTopLeftRadius: entryBorderRadius,
    borderTopRightRadius: entryBorderRadius,
    height: wp(52),
    overflow: 'hidden'
  },
  topInfo: {
    width: "100%",
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop:20,
    paddingLeft:25,
    paddingRight:25,
  },
  boxCategory: {
    height:28,
    backgroundColor: 'rgba(0,0,0,0.7)',
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 13,
    paddingRight: 13,
    borderRadius:4,
  },
  category: {
    color: '#fff',
    fontFamily: 'nunito-regular',
    fontSize: 14,
  },
  buttonLike: {
    marginTop:-5,
    marginRight:-3,
  },
  likeImage: {
    width: wp(11),
    height: wp(11),
  },
  boxPrice: {
    position: 'absolute',
    right: 25,
    top: wp(46),
    elevation: 5,
    backgroundColor: '#fff',
    paddingTop: 6,
    paddingBottom: 7,
    paddingLeft: 17,
    paddingRight: 17,
    borderRadius:10,
    flexDirection: 'row',

    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
  },
  price: {
    color: '#429e32',
    fontFamily: 'nunito-bold',
    fontSize: 19,
  },
  priceUnit:{
    color:'#4d4d4d',
    fontFamily: 'nunito-regular',
    fontSize: 16,
    paddingTop:3,
  },
  boxDetail: {
    paddingTop:20,
    paddingBottom:25,
    paddingLeft: 25,
    paddingRight: 25,
  },
  directionImage: {
    width: wp(4.26667),
    height: wp(4.26667),
    marginTop: 2
  },
  title: {
    marginBottom: 13,
    fontSize: 18,
    lineHeight: 28,
    color: '#000',
    fontFamily: 'nunito-bold',
    height:56,
  },
  boxLocation: {
    flexDirection: 'row',
  },
  location: {
    color: '#515050',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  direction: {
    marginTop:3,
    color: '#a4a4a4',
    fontFamily: 'nunito-regular',
    fontSize: 13,
  }
})


class BoxListing extends Component {
  render() {
    const data = this.props.data
    const likedImage = data.liked ? require('../images/saved_listings/icon-like_active.png') : require('../images/home/icon-like.png')
    
    const price_unit = data.price_unit ? (
        <Text style={styles.priceUnit}>
            { ' ' + data.price_unit }
        </Text>
    ) : false;

    const placeholder_image = data.image ? data.image : {uri: data.placeholder_photo_url}
    const safe_address = data.safe_address
    const distance_from = parseInt(data.distance_from/1000) + ' km away from you'

    return (
      <TouchableOpacity activeOpacity={1} style={[styles.boxContainer, this.props.boxContainerStyle]}
        onPress={this.props.onPress}>
        <View style={[styles.box, this.props.boxStyle]}>
          <ImageBackground style={[styles.boxImage, this.props.boxImageStyle]} source={placeholder_image} />
          <View style={styles.topInfo}>
            <View style={styles.boxCategory}>
              <Text style={styles.category}>{data.category.name}</Text>
            </View>
            <TouchableWithoutFeedback>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonLike} onPress={this.props.onToggleLike}>
                <Image style={styles.likeImage} source={likedImage}/>
              </TouchableOpacity>
            </TouchableWithoutFeedback>
          </View>

          <View style={[styles.boxPrice, this.props.boxPriceStyle]}>
            <Text style={styles.price}>{data.price}</Text>
            {price_unit}
          </View>

          <View style={styles.boxDetail}>
            <Text style={styles.title}>{data.title}</Text>

            <View style={styles.boxLocation}>
              <Image style={styles.directionImage} source={require('../images/home/icon-location.png')}/>
              <View style={{marginLeft: 10}}>
                <Text style={styles.location}>{safe_address}</Text>
                <Text style={styles.direction}>{distance_from}</Text>
              </View>
            </View>

          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

export default BoxListing
