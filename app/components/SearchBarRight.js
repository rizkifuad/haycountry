import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View, 
  Dimensions,
} from 'react-native'

import { wp } from '../utils'
class Screen extends Component {

  render() {
    return (
        <TouchableOpacity activeOpacity={0.75} style={styles.buttonFilters} onPress={this.props.onPress}>
          <Image style={styles.filters} source={this.props.image}/>
        </TouchableOpacity>
    )
  }

}

const styles = StyleSheet.create({
  buttonFilters: {
    marginRight: wp(6.6667),
  },
  filters: {
    width: 30,
    height: 30,
  }
})

export default Screen
