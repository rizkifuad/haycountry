import React, { Component } from 'react';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';
import BoxListing from '../components/BoxListing';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'
import { horses } from '../data'

class Screen extends Component {

  constructor(props) {
    super(props)
    this.state = { data: horses }
  }

  toggleLike(key) {
    let items = this.state.data
    items[key].liked = !items[key].liked
    this.setState({data: items})
  }


  render() {
    return (
      <View style={styles.container}>
          {
            this.state.data.map((d, i) => {
              return <BoxListing data={d} key={i} onToggleLike={() => {this.toggleLike(i)}} onPress={() => this.props.goToListing()} />
            })
          }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
})

export default Screen
