import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  Dimensions,
} from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { wp }  from '../utils'

class Screen extends Component {
  static navigatorStyle = {
    drawUnderNavBar: true,
    navBarHidden: true
  };

  constructor() {
    super()
    this.state = {
      entries: [
      {illustration: require('../images/onboarding/illustration.png')},
      {illustration: require('../images/onboarding/illustration.png')},
      {illustration: require('../images/onboarding/illustration.png')}],
      activeSlide: 0,
    };
  }

  handleSignIn = () => {
    this.props.navigator.showModal({
      screen: 'SignIn',
      title: 'Sign In',
    });
  };

  _renderItem ({item, index}) {
    return (
      <View style={styles.carouselItem}>
        <Image style={styles.splashImage} source={item.illustration}/>
      </View>
    );
  }
  get pagination () {
    const { entries, activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={entries.length}
        activeDotIndex={activeSlide}
        containerStyle={{ paddingVertical: 20 }}
        dotContainerStyle={{ marginHorizontal: -10 }}
        dotStyle={{
            marginHorizontal: -10,
        }}
        dotElement={<Image style={styles.sliderIndicator} source={require("../images/onboarding/icon-dotactive.png")}/>}
        inactiveDotElement={<Image style={styles.sliderIndicator} source={require("../images/onboarding/icon-dot.png")}/>}
        carouselRef={this._carousel}
        tappableDots={!!this._carousel}
      />
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Image resizeMode="cover" style={styles.background} source={require("../images/onboarding/bg.png")}/>

        <View style={styles.carouselContainer}>
          <Carousel
            layout={'default'}
            ref={(c) => { this._carousel = c; }}
            data={this.state.entries}
            renderItem={this._renderItem}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
            onSnapToItem={(index) => this.setState({ activeSlide: index }) }
          />
          { this.pagination }
        </View>

        <View style={styles.header}>
          <Text style={styles.textHeader}>Search Between </Text><Text style={styles.textHeaderBlue}>1.500 </Text>
          <Text style={styles.textHeader}>listings</Text>
        </View>
        <Text style={styles.textDescription}>Search between 1.500 various listings in various categories.</Text>

        <TouchableOpacity activeOpacity={0.75} style={styles.button} onPress={() => this.props.navigation.navigate('SignIn')}>
          <Image style={styles.buttonImage} source={require("../images/onboarding/btn-green.png")}/>
          <Text style={styles.buttonText}>SIGN IN</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const sliderWidth = width;
const itemWidth= width;
const styles = StyleSheet.create({
  carouselContainer:{
    height: wp(87),
  },
  carouselItem:{
    width: width,
    height: wp(66),
    alignItems: 'center',
  },
  splashImage: {
    width: wp(86.13334),
    height: wp(65.73334)
  },
  sliderIndicator: {
    width: 40,
    height: 40
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  sliderButton: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 30
  },
  header: {
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 5
  },
  button: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonImage: {
    width: 224,
    height: 89
  },
  buttonText: {
    position: 'absolute',
    color: '#fff',
    top: 21,
    textAlign: 'center',
    fontFamily: 'nunito-bold',
    fontSize: 20,
  },
  textHeader: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'nunito-bold',
    color: '#000'
  },
  textHeaderBlue: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'nunito-bold',
    color: '#1264ca'
  },
  textDescription: {
    marginTop: 10,
    fontSize: 18,
    textAlign: 'center',
    fontFamily: "nunito-regular",
    color:'#4e4e4e',
    paddingLeft: 20,
    paddingRight: 20,
    lineHeight: 28
  },
  background: {
    position: 'absolute',
    top: 0,
    zIndex: -1,
    width: width,
    height: height,
  }
})

export default Screen;
