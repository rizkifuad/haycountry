import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import ButtonBottom from '../components/ButtonBottom';
import FormInput from '../components/FormInput';
import { TextInputMask } from 'react-native-masked-text'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView,
  Dimensions
} from 'react-native'
import Modal from 'react-native-modal';

import { wp, width, height }  from '../utils'

import { headerStyle } from '../styles';

class UpgradeBox extends Component {
  render() {
    const radio = {
      active: require('../images/plan_upgrade/icon-radio_active.png'),
      inactive: require('../images/plan_upgrade/icon-radio.png'),
    }

    const radioImage = this.props.active ? radio.active : radio.inactive

    return (
      <View style={styles.upgradeBoxWrapper}>
        <View style={styles.canUpgrade} collapsable={false}>
          <Image resizeMode="cover" style={styles.bgCard} source={require('../images/plan_upgrade/bg-card.png')}/>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.greenText}>{this.props.name}</Text>
            <Text style={styles.price}>({this.props.price})</Text>
          </View>
          <Text style={styles.planDesc}>
            <Text>{this.props.description}</Text>
          </Text>

        </View>

        <TouchableOpacity activeOpacity={0.75} style={styles.buttonRadio} onPress={this.props.onPress}>
          <Image style={styles.radioImage} source={radioImage} />
        </TouchableOpacity>

      </View>
    )
  }
}

class Screen extends Component {
  static navigationOptions = {
    headerTitle: <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='Plan Upgrade'/>
      </View>,
    headerStyle: headerStyle,
    headerRight: <View/>
  }

  constructor(props) {
    super(props)
    this.state = {
      activeUpgradePlan: 0,
      isModalVisible: false,
      cardNumber: '1523215665465486',
      cardNumberHidden: '15235486',
      borderColor: '#d3d3d3', 
      ccEditable: false,
    }
    this.changeUpgradePlan = this.changeUpgradePlan.bind(this)
    this._showModal = this._showModal.bind(this)
    this._hideModal = this._hideModal.bind(this)
  }

  changeUpgradePlan(key) {
    this.setState({activeUpgradePlan: key})
  }

  _showModal() {
    this.setState({ isModalVisible: true })
  }

  _hideModal() {
    this.setState({ isModalVisible: false })
  }
  _onModalShow = () => {
    
  }
  _onModalHide = () => {

  }

  render() {
    const upgradeTo = [{
      name: 'Business',
      price: '$14.99/mo',
      description: 'Up to 15 listings',
      btnText: 'Upgrade To Business ($14.99)',
    }, {
      name: 'Enterprise',
      price: '$24.99/mo',
      description: 'Unlimited listings',
      btnText: 'Upgrade To Enterprise ($24.99)',
    }]

    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>

          <Text style={styles.header}>Your current plan:</Text>

          <View style={styles.currentPlan}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.greenText}>Professional</Text>
              <Text style={styles.price}>($7.99/mo)</Text>
            </View>

            <Text style={styles.planDesc}>
              <Text>Up to 5 listings</Text>
              <Text style={styles.greyText}> (2 listings left)</Text>
            </Text>

            <Text style={styles.expires}>
              <Text style={styles.redText}>Expires on</Text>
              <Text> 25 June, 2018</Text>
            </Text>
          </View>


          <Text style={styles.header}>Can upgrade to:</Text>
          <View style={{marginBottom:30}}>
          {
              upgradeTo.map((u, i) => {
                return <UpgradeBox key={u.name} name={u.name} price={u.price} description={u.description} active={this.state.activeUpgradePlan === i} onPress={() => {this.changeUpgradePlan(i)}} />
              })
          }
          </View>

        </ScrollView>

        <ButtonBottom text={upgradeTo[this.state.activeUpgradePlan].btnText} image={require('../images/contact_seller/btn-green.png')} onPress={this._showModal}/>

        <Modal isVisible={this.state.isModalVisible} style={styles.modalContent} useNativeDriver={true} onBackButtonPress={this._hideModal} onModalShow={() => this._onModalShow()} onModalHide={() => this._onModalHide()}>
          <TouchableOpacity activeOpacity={0.75} style={styles.buttonClose} onPress={this._hideModal}>
            <Image style={styles.closeImage} source={require("../images/contact_seller/icon-close.png")}/>
          </TouchableOpacity>

          <Text style={styles.upgradeText}>{upgradeTo[this.state.activeUpgradePlan].name+' Plan'}</Text>
          <Text style={styles.upgradePrice}>{upgradeTo[this.state.activeUpgradePlan].price}</Text>

          <View style={{width: wpScreen(86.66666)}}>
            <Text style={styles.label}>Card Number</Text>
            <TextInputMask 
              ref='ccInput'
              underlineColorAndroid='rgba(255,255,255,0)'
              type={'credit-card'}
              value={(this.state.ccEditable) ? this.state.cardNumber : this.state.cardNumberHidden}
              style={[styles.input]}
              editable={this.state.ccEditable}
              options={{
                obfuscated: !this.state.ccEditable
              }}
            />
            <Image style={styles.visa} source={require('../images/billing_info/visa.png')}/>
            <TouchableOpacity activeOpacity={0.75} style={styles.buttonEdit} onPress={() => this.setState({ccEditable: true}) }>
              <Image style={styles.editImage} source={require("../images/payment/icon-pencil.png")}/>
            </TouchableOpacity>
          </View>

          <TouchableOpacity activeOpacity={0.75} style={styles.buttonPurchase}>
            <Image style={styles.purchaseImage} source={require("../images/payment/btn-green-round-sm.png")}/>
            <Text style={styles.purchaseText}>Confirm Purchase</Text>
          </TouchableOpacity>
        </Modal>
      </View>
    )
  }
}

const widthScreen = Dimensions.get('window').width;
function wpScreen(percentage){
  const value = (percentage * widthScreen) / 100;
  return Math.round(value);
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    flex:1,
    width: wp(100),
  },
  header: {
    marginLeft: wp(8),
    marginTop: 40,
    marginBottom: 10,
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 14,
  },
  currentPlan: {
    marginTop: 15,
    marginLeft: wp(8),
    marginRight: wp(8),
    backgroundColor: '#f5f5f5',
    paddingTop:25,
    paddingBottom: 35,
    paddingLeft: 40,
    paddingRight: 40,
    width: wp(84),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:8
  },
  greenText: {
    color: '#53a235',
    fontSize: 26,
    fontFamily: 'nunito-bold',
  },
  price: {
    color: '#000',
    fontSize: 20,
    fontFamily: 'nunito-regular',
    marginLeft: 5,
    paddingTop: 2
  },
  planDesc: {
    marginTop: 12,
    color: '#000',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  greyText: {
    color: '#ccc',
  },
  expires: {
    marginTop: 30,
    color: '#000',
    fontSize: 14,
    fontFamily: 'nunito-regular',
  },
  redText: {
    color: 'red',
  },
  upgradeBoxWrapper: {
    paddingBottom:wp(2.6),
    alignItems: 'center',
  },
  canUpgrade: {
    alignItems: 'center',
    paddingTop:40,
    paddingLeft:40,
    paddingRight:40,
    width:wp(96.93334),
    height:wp(45.06667),
  },
  bgCard:{
    width:wp(96.93334),
    height:wp(45.06667),
    position:'absolute',
    left:0,
    top:0,
  },
  buttonRadio: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
    left: wp(42),
    elevation:0,
  },
  radioImage: {
    width: wp(16),
    height: wp(16)
  },
  modalContent:{
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: undefined,
    margin: 0,
  },
  upgradeText:{
    color: '#000',
    fontSize: 27,
    fontFamily: 'nunito-bold',
    marginTop:100,
    marginBottom:5,
  },
  upgradePrice:{
    color: '#53a235',
    fontSize: 19,
    fontFamily: 'nunito-bold',
    marginBottom:25,
  },
  buttonPurchase: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:50,
  },
  purchaseImage: {
    width: wp(56.8),
    height: wp(16.13334)
  },
  purchaseText: {
    position: 'absolute',
    textAlign: 'center',
    fontFamily: 'nunito-regular',
    fontSize: 18,
    color:'#fff'
  },
  buttonClose:{
    position:'absolute',
    right:25,
    top:25,
  },
  closeImage:{
    width:30,
    height:30
  },
  label: {
    fontFamily: 'nunito-bold',
    fontSize:14,
  },
  input: {
    marginTop: 0,
    paddingTop: 0,
    marginLeft: 0,
    paddingLeft: 0,
    paddingBottom: 14,
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    textAlign: 'left',
    fontSize: 19,
    color:'#000',
    fontFamily: 'nunito-regular',
  },
  visa: {
    position: 'absolute',
    top: 26,
    left: 202,
    width: 22,
    height: 14,
  },
  buttonEdit: {
    position: 'absolute',
    right:0,
    top:15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  editImage: {
    width: 25,
    height: 25
  },
})

export default Screen
